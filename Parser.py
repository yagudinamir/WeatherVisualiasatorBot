import requests
import urllib.request
import re
from bs4 import BeautifulSoup
import Config

# query ='Погода+Саратов'
# print(soup.prettify())


def get_wheather(query):
    KEY = Config.key

    HEADERS = Config.headers

    html = requests.get(
        'https://www.google.ru/search?q={}&cr=countryRU&lr=lang_ru&key={}'.format(urllib.parse.quote(query), KEY),
        headers=HEADERS)
    soup = BeautifulSoup(html.text, 'html.parser')
    tmp = re.findall(r'[0-9]+', str(soup.find(id="wob_tm")))[0]
    rain = re.findall(r'[0-9]+%', str(soup.find(id="wob_pp")))[0]
    vl = re.findall(r'[0-9]+%', str(soup.find(id="wob_hm")))[0]
    wind = soup.find(id="wob_ws").text
    image = "https:" + soup.find(id="wob_tci")['src']
    text = soup.find(id="wob_dc").text
    return {'tempr' : tmp, 'rain' : rain, 'vlag' : vl, 'windy' : wind, 'image' : image, 'text' : text}

# print(get_wheather(query))