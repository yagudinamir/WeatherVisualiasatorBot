from telegram.ext import Updater
from telegram.ext import MessageHandler, Filters
from telegram.ext import CommandHandler, RegexHandler, ConversationHandler, CallbackQueryHandler
import telegram
import logging
import Config
from enum import Enum
import Parser

WEATHER, MENU = range(2)

logging.basicConfig(format='$(asctime)s - %(name)s - %(levelname)s - %(message)s',
                         level=logging.INFO)

logger = logging.getLogger(__name__)


def build_menu(buttons, n_cols):
    men = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    return men


def start(bot, update):
    update.message.reply_text("Hello! This is a WeatherVisualisatorBot,\nPlease type /menu\nto choose the city to know more about weather in it")
    return MENU


def menu(bot, update):
    weather_keyboard = [["Москва"], ["Казань"], ["Choose another one"]]
    reply_markup = telegram.ReplyKeyboardMarkup(weather_keyboard, one_time_keyboard=True)
    update.message.reply_text("Please choose your option:", reply_markup=reply_markup)
    return WEATHER


def wheather(bot, update):
    option = update.message.text
    query = 'Погода+'
    if option == 'Choose another one':
        update.message.reply_text("Please type your city in Russian")
        return WEATHER
    elif option == 'Москва':
        query += option
    elif option == 'Казань':
        query += option
    else:
        query += option
    try:
        current_wh = Parser.get_wheather(query)
    except BaseException:
        update.message.reply_text("Please type correct city name!", reply_markup=telegram.ReplyKeyboardRemove())
        return 1
    text = "На данный в вашем городе " + current_wh['text'].lower() + "\n"
    text += "Температура " + current_wh['tempr'] + " градусов цельсия" + "\n"
    probability = "маловероятны"
    if int(current_wh['rain'][:-1]) > 50:
        probability = "вероятны"
    text += "Осадки " + probability + "\n"
    text += "Влажность " + current_wh['vlag'] + "\n"
    text += "Скорость ветра " + current_wh['windy']
    update.message.reply_text(text, reply_markup=telegram.ReplyKeyboardRemove())
    bot.send_photo(chat_id=update.message.chat_id, photo=current_wh['image'])
    bot.send_message(chat_id=update.message.chat_id, text="type /menu for another one\n type /help for help")
    return MENU


def cancel(bot, update):
    update.message.reply_text("See you soon...", reply_markup=telegram.ReplyKeyboardRemove())
    return ConversationHandler.END


def error(bot, update, error):
    bot.send_message(chat_id=update.message.chat_id, text="Sorry couldn't understand you")
    logger.warning('Update "%s" caused error "%s"', update, error)


def help(bot, update):
    update.message.reply_text("A WeatherVisualizatorBot\ntype /menu to choose the city\ntype /cancel to leave")


def main():
    bot = Updater(token=Config.token)
    dispatcher = bot.dispatcher

    conversation_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            MENU: [CommandHandler('menu', menu)],
            WEATHER: [MessageHandler(Filters.text, wheather)]
        },
        fallbacks=[CommandHandler('help', help), CommandHandler('cancel', cancel), CommandHandler('menu', menu)]
    )

    dispatcher.add_handler(conversation_handler)

    dispatcher.add_error_handler(error)

    bot.start_polling()

    bot.idle()


if __name__ == '__main__':
    main()